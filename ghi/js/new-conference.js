
window.addEventListener('DOMContentLoaded', async() => {
    const apiUrl = 'http://localhost:8000/api/locations/';

    try {
        const response = await fetch(apiUrl);
        if (!response.ok) {
            throw new Error('Error' + response.status);
        }

        const data = await response.json();
        const locations = data.locations;
        const selectElement = document.getElementById('location');

        for (let location of locations){
            const option = document.createElement('option');
            option.value = location.id;
            // (option.label and option.innerHTML works the same way for this case
            // change option.label to option.innerHTML later if it crashes)
            // option.label = location.name;
            option.innerHTML = location.name
            selectElement.appendChild(option);
        }
    }
    catch (e) {
        console.error('Error:' , e)
    }
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        }
    }

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
        newConferenceFormTag.reset();
        const newConference = await response.json();
        console.log(newConference);
    }

})
});
