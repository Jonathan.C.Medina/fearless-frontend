// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';
//     try {}
//     const response = await fetch(url);
//     console.log(response);

//     const data = await response.json();
//     console.log(data);
// });

// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';
//     try {
//     const response = await fetch(url);

//     if (!response.ok) {
//         throw new Error('Error: ' + response.status);
//        // Figure out what to do when the response is bad
//     } else {
//         const data = await response.json();
//         // console.log(data);

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;
//         // console.log(conference);


//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             // console.log(details);

//             const description = details.conference.description;
//             const descriptionTag = document.querySelector('.card-text');
//             descriptionTag.innerHTML = description;

//             const imageTag = document.querySelector('.card-img-top');
//             imageTag.src = details.conference.location.picture_url;

//             console.log(details);
//         }
//     }
//     } catch (e) {
//         console.error('Error: ', e);
//       // Figure out what to do if an error is raised
//     }

// });


window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, starts, ends, location) {
        const formatStartDate = formatDate(starts);
        const formatEndDate = formatDate(ends);

        return `
        <div class="col-md-4">
            <div class="shadow-lg p-3 mb-5 bg-white rounded">
                <div class="card">
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${name}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                        <p class="card-text">${description}</p>
                    <div class="card-footer">
                        ${formatStartDate} - ${formatEndDate}
                    </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    }

    function formatDate(dateString){
        const date = new Date(dateString);
        return date.toLocaleDateString();
    }

    // function createCard(name, description, pictureUrl) {
    //     return `
    //         <div class="card">
    //             <img src="${pictureUrl}" class="card-img-top">
    //             <div class="card-body">
    //                 <h5 class="card-title">${name}</h5>
    //                 <p class="card-text">${description}</p>
    //             </div>
    //         </div>
    //     `;
    // }

    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
        // Figure out what to do when the response is bad
            throw new Error('Error: ' + response.status);
        } else {
            const data = await response.json();
            const column = document.querySelector('.row')

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const location = details.conference.location.name;
                const pictureUrl = details.conference.location.picture_url;
                const starts = new Date(details.conference.starts).toDateString();
                const ends = new Date(details.conference.ends).toDateString();
                const html = createCard(name, description, pictureUrl, starts, ends, location);
                // console.log(html);
                const div = document.createElement('div');
                div.innerHTML = html.trim();
                column.appendChild(div.firstChild);

                // const column = document.querySelector('.col');
                // column.innerHTML += html;
        }
        }
    }
    } catch (e) {
      // Figure out what to do if an error is raised
        console.error('Error: ', e);
    }
});
